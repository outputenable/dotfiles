" ~/.vimrc

syntax enable
colorscheme elflord

" Example from :help statusline
" Emulate standard status line with 'ruler' set
set statusline=%<%f\ %h%m%r%=%-14.(%l,%c%V%)\ %P
set laststatus=2

" Change colors of status line depending on mode
autocmd InsertEnter * highlight StatusLine cterm=bold ctermbg=DarkGreen ctermfg=White
autocmd InsertLeave * highlight StatusLine cterm=bold ctermbg=LightGray ctermfg=Black
highlight StatusLine cterm=bold ctermbg=LightGray ctermfg=Black

" Vim's absolute, relative and hybrid line numbers
" https://jeffkreeftmeijer.com/vim-number/
set number relativenumber
augroup numbertoggle
  autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter * set norelativenumber
augroup END

" Set cursor shapes
let &t_SI = "\<Esc>[6 q"  " Insert mode: vertical bar cursor
let &t_SR = "\<Esc>[4 q"  " Replace mode: underline cursor
let &t_EI = "\<Esc>[2 q"  " Normal mode: block cursor

set expandtab
set autoindent

set ignorecase

set fixendofline

noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

" ClangFormat
" https://clang.llvm.org/docs/ClangFormat.html
function ClangFormatFile()
  let l:lines = "all"
  py3file /usr/share/clang/clang-format-11/clang-format.py
endfunction

autocmd BufWritePre *.h,*.cc,*.cpp,*.hpp call ClangFormatFile()

" https://github.com/rust-lang/rust.vim
let g:rustfmt_autosave = 1
