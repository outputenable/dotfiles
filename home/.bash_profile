# ~/.bash_profile

# user-specific bin directories
for p in \
   "$HOME/.local/bin" \
   "$HOME/bin"
do
   if [ -d "$p" ]
   then
      PATH="$p${PATH:+:$PATH}"
   fi
done

export PATH

if [ -f ~/.bashrc ]
then
   source ~/.bashrc
fi
