# ~/.bashrc

if [ -f /etc/bashrc ]
then
   source /etc/bashrc
fi

if [[ $(id -u) -eq 0 ]]
then
   umask 0077
else
   umask 0022
fi

# don't put lines starting with space in the history
HISTCONTROL=ignorespace

# unlimited history size
HISTSIZE=-1

HISTIGNORE='clear:l:la:ll:lla:ls:pwd'

# associate a time stamp with each history entry
# this is the format string used for strftime(3),
# e.g. Sat 2015-06-27 12:39:00 +0200
HISTTIMEFORMAT='%a %F %T %z '

# save all lines of a multiple-line command in the same history entry
shopt -s cmdhist

shopt -s histappend

# do not immediately execute results of history substitution
shopt -s histverify

# https://unix.stackexchange.com/questions/73484/how-can-i-set-vi-as-my-default-editor-in-unix
# https://unix.stackexchange.com/questions/4859/visual-vs-editor-what-s-the-difference
export VISUAL="$(command -v vim)"
export EDITOR="$VISUAL"

export LESS='--no-init --quit-if-one-screen --RAW-CONTROL-CHARS'

# homeshick, https://github.com/andsens/homeshick
source "$HOME/.homesick/repos/homeshick/homeshick.sh"
source "$HOME/.homesick/repos/homeshick/completions/homeshick-completion.bash"

# Subversion
if command -v svn >/dev/null 2>&1
then
   source ~/.bashrc_subversion
fi

# Git prompt
for p in \
   /usr/share/git-core/contrib/completion/git-prompt.sh \
   /usr/lib/git-core/git-sh-prompt
do
   if [[ -e $p ]]
   then
      source "$p"
      break
   fi
done

unset PS1
export PROMPT_COMMAND=__prompt_command

export VIRTUAL_ENV_DISABLE_PROMPT=1

__prompt_command() {
   local last_exit_status=$?

   local creset='\[\e[0m\]'
   local clred='\[\e[1;31m\]'
   local cgreen='\[\e[0;32m\]'
   local ccyan='\[\e[1;36m\]'

   PS1=''

   if [[ $VIRTUAL_ENV ]]
   then
      PS1+="[$ccyan$(basename "$VIRTUAL_ENV")$creset] "
   fi

   PS1+='[@\h'

   if declare -F __git_ps1 > /dev/null
   then
      PS1+="$(__git_ps1 ' (Git:%s)')"
   fi

   PS1+=' \W]'

   if [[ $last_exit_status -ne 0 ]]
   then
      PS1+="$clred$last_exit_status\$"
   else
      PS1+="$cgreen\$"
   fi

   PS1+="$creset "
}

if [ -f ~/.bash_aliases ]
then
   source ~/.bash_aliases
fi

if [ -f ~/.bashrc_local ]
then
   source ~/.bashrc_local
fi

if [ "$(id -u)" -eq 0 ]
then
   alias rm='rm -i'
   alias cp='cp -i'
   alias mv='mv -i'
fi
