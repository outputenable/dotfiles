;;; ~/.config/emacs/init.el

(setq inhibit-startup-screen t)

(setq custom-file (locate-user-emacs-file "custom.el"))
(load custom-file 'noerror 'nomessage)

;; Turn off some unneeded UI elements/behavior.
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(tooltip-mode -1)

;; Don't pop up UI dialogs when prompting.
(setq use-dialog-box nil)

(setq visible-bell t)

(save-place-mode 1)

(global-auto-revert-mode 1)

;; Display line and column number in mode line.
(line-number-mode 1)
(column-number-mode 1)

(global-display-line-numbers-mode 1)

(setq-default backward-delete-char-untabify-method nil)

(customize-set-variable 'require-final-newline t)
(customize-set-variable 'file-preserve-symlinks-on-save t)

(load-theme 'deeper-blue t)

;; EmacsWiki: Calendar Localization (German)
;; https://www.emacswiki.org/emacs/CalendarLocalization
(customize-set-variable 'calendar-date-style 'iso)
(setq calendar-week-start-day 1
      calendar-day-name-array ["Sonntag" "Montag" "Dienstag" "Mittwoch"
                               "Donnerstag" "Freitag" "Samstag"]
      calendar-month-name-array ["Januar" "Februar" "März" "April"
                                 "Mai" "Juni" "Juli" "August"
                                 "September" "Oktober" "November" "Dezember"])

(setq holiday-general-holidays
      '((holiday-fixed 10 3 "Tag der Deutschen Einheit")))
(setq holiday-local-holidays
      '((holiday-fixed 1 1 "Neujahr")
	(holiday-easter-etc  -2 "Karfreitag")
	(holiday-easter-etc  +1 "Ostermontag")
	(holiday-fixed 5 1 "Tag der Arbeit")
	(holiday-easter-etc +39 "Christi Himmelfahrt")
	(holiday-easter-etc +50 "Pfingstmontag")
	(holiday-easter-etc +60 "Fronleichnam")
	(holiday-fixed 12 25 "1. Weihnachtsfeiertag")
	(holiday-fixed 12 26 "2. Weihnachtsfeiertag")))

(require `package)

(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)

(require 'use-package)

(customize-set-variable 'use-package-always-defer t)

;; https://www.reddit.com/r/emacs/comments/np6ey4/how_packageel_works_with_use_package/

(use-package cider)

(use-package clojure-mode)

(use-package git-commit
  :demand t)

(use-package haskell-mode)

; https://github.com/ledger/ledger-mode
(use-package ledger-mode)

(use-package org
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . nil)
     (shell . t))))

;; Basic setup from System Crafters: Getting Started with Org Roam - Build a
;; Second Brain in Emacs, https://www.youtube.com/watch?v=AyhPmypHDEw
(use-package org-roam
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/notes")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert)
	 ("C-c n t" . org-roam-tag-add)
	 :map org-mode-map
	 ("C-M-i"   . completion-at-point))
  :config
  (org-roam-setup))

(use-package rust-mode
  :hook (rust-mode . (lambda () (setq indent-tabs-mode nil)))
  :config
  (setq rust-format-on-save t))

;; EmacsWiki: Indenting C,
;; https://www.emacswiki.org/emacs/IndentingC
(setq-default c-basic-offset 8
              tab-width 8
              indent-tabs-mode t)

(setq-default c-syntactic-indentation nil)  ; disable for now
